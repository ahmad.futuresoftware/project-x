package com.futuresoftware;


import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.ForEach;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.futuresoftware.security.credentials.entities.Authority;
import com.futuresoftware.security.credentials.entities.User;
import com.futuresoftware.security.credentials.service.UserService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectXApplicationTests {

	@Autowired
	UserService userService;
	
	
	@Test
	public void testgetById() {

		User userById = userService.getUserById(2);
		
		
		String userName = userById.getUserName();
		String password = userById.getPassword();
		boolean isActive = userById.isEnabled();
		
		List<Authority> authorityList = userById.getAuthorityList();
		
		System.out.print(userName+" "+password+ " "+isActive+" ");
		
		for (Authority authority : authorityList) {
			
			System.out.print(authority.getRole()+" ");
		}
		
	
        
	}
	
	@Test
	public void testInsert() {

		
        
	}
	
	
	@Test
	public void testUpdate() {

		
        
	}
	
	@Test
	public void testDeleteById() {

		
        
	}
	

	
	@Test
	public void testGetAll() {
		
		
        
	}
	
	
	
	
	
	
	
	
	
	
	

}
