<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>New User Registration Form</title>
</head>
<body>

<Form action="saveUser" method="post">

<table>

<tr>
<td>First Name:</td>
<td></td>
<td><input type="text" name="firstName"/></td>
</tr>

<tr>
<td>Last Name:</td>
<td></td>
<td><input type="text" name="lastName"/></td>
</tr>

<tr>
<td>User Name:</td>
<td></td>
<td><input type="text" name="userName"/></td>
</tr>

<tr>
<td>E-Mail:</td>
<td></td>
<td><input type="text" name="eMail"/></td>
</tr>

<tr>
<td>Password:</td>
<td></td>
<td><input type="text" name="password"/></td>
</tr>

<tr>
<td></td>
<td/>
<td align="center"><input type="submit" value="Save"/></td>

</tr>

</table>
</Form>
<br><br>
${msg}  <a href="showLoginForm">Go To Login Page</a>
<br><br>
Already Registered? <a href="showLoginForm">Go To Login Page</a>
</body>
</html>