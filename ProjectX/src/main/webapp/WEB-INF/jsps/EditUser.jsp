<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


	<h2>Edit User</h2>

	<form action="saveUpdatedUser" method="post">

		<table>

			<tr>
				<td>ID</td>
				<td><input type="text" name="id" value="${editableUser.id}" readonly /></td>
			</tr>
			
			<tr>
				<td>First Name</td>
				<td><input type="text" name="firstName" value="${editableUser.firstName}" /></td>
			</tr>
			
			<tr>
				<td>Last Name</td>
				<td><input type="text" name="lastName" value="${editableUser.lastName}" /></td>
			</tr>

	
			<tr>
				<td>User Name</td>
				<td><input type="text" name="userName" value="${editableUser.userName}" /></td>
			</tr>
			
			<tr>
				<td>E-Mail</td>
				<td><input type="text" name="eMail" value="${editableUser.eMail}" /></td>
			</tr>
			
			<tr>
				<td>Password</td>
				<td><input type="text" name="password" value="${editableUser.password}" /></td>
			</tr>
			
			<tr>
				<td></td>
				<td align="center"><input type="submit" value="Save" /></td>
			</tr>
			
		</table>

	</form>
</body>


















</html>