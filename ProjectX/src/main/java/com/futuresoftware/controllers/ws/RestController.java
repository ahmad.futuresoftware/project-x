package com.futuresoftware.controllers.ws;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.*;
import com.futuresoftware.security.credentials.entities.User;
import com.futuresoftware.security.credentials.service.UserService;

@org.springframework.web.bind.annotation.RestController
@RequestMapping(value= "/users" , 
				produces = MediaType.APPLICATION_JSON_VALUE, 
				consumes = MediaType.APPLICATION_JSON_VALUE )

public class RestController {

	@Autowired
	UserService userService;
	

	
	@GetMapping()
	public List<User> getAllUsers() {

		return userService.getAllUsers();

	}

	@GetMapping("/{id}")
	
	public User getUserById(@PathVariable(name = "id") int id) {

		return userService.getUserById(id);

	}

	
	@PutMapping
	public User updateUser(@RequestBody User user) {

		return userService.updateUser(user);

	}


	@DeleteMapping("/{id}")
	public void deleteUser(@PathVariable(name = "id") int id) {

		userService.deleteUserById(id);

	}

	
	@PostMapping
	public User insertUser(@RequestBody User user) {

		return userService.insertUser(user);

	}

}
