package com.futuresoftware.security.credentials.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.futuresoftware.security.credentials.entities.User;

public interface UserRepo extends JpaRepository<User, Integer> {
	
	public User findUserByUserName(String username);
	

}
