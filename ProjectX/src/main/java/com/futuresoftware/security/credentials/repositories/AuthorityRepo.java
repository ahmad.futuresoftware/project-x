package com.futuresoftware.security.credentials.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.futuresoftware.security.credentials.entities.Authority;

public interface AuthorityRepo extends JpaRepository<Authority, Integer> {

}
