package com.futuresoftware.security.credentials.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.futuresoftware.security.credentials.entities.Authority;
import com.futuresoftware.security.credentials.repositories.AuthorityRepo;

@Service
public class AuthorityServiceImpl implements AuthorityService {

	@Autowired
	AuthorityRepo authRepo;
	
	@Override
	public Authority insertAuthority(Authority authority) {
		return authRepo.save(authority);
	}

	@Override
	public Authority updateAuthority(Authority authority) {
		return authRepo.save(authority);
	}

	@Override
	public void deleteAuthorityById(int id) {
		
		authRepo.deleteById(id);
		
	}

	@Override
	public Authority getAuthorityById(int id) {
		return authRepo.findById(id).orElse(null);
	}

	@Override
	public List<Authority> getAllAuthorities() {
		return authRepo.findAll();
	}

}
