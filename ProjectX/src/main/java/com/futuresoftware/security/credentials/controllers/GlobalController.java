package com.futuresoftware.security.credentials.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GlobalController {
	
	@RequestMapping("/")
	public String defaultView() {
		
		return "all";
	}
	
	@RequestMapping("/admin")
	public String adminView() {
		
		return "admin";
	}
	
	@RequestMapping("/user")
	public String userView() {
		
		return "user";
	}

}
