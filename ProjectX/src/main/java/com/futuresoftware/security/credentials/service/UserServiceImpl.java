package com.futuresoftware.security.credentials.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.futuresoftware.security.credentials.entities.User;
import com.futuresoftware.security.credentials.repositories.UserRepo;

@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepo userRepo;

	@Override
	public User insertUser(User user) {
		return userRepo.save(user);
	}

	@Override
	public User updateUser(User user) {
		return userRepo.save(user);
	}

	@Override
	public void deleteUserById(int id) {
		
		userRepo.deleteById(id);
		
	}

	@Override
	public User getUserById(int id) {
		return userRepo.findById(id).orElse(null);
	}

	@Override
	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public User getUserByUserName(String username) {
		
		return userRepo.findUserByUserName(username);
	}

}
